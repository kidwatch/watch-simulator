# watch-simulator

Kid Watch Simulator for testing UDP server

使用方法：

```bash
$ ./watch-simulator -s=server_ip:port -w=imei_number -d=0.001 -logtostderr -v=4
```

参数说明：


```bash
$ ./watch-simulator -h
```
> -d	坐标增量

> -h	心跳周期(s), 默认30

> -i	位置上报周期(s), 默认30

> -p1	开始位置，默认为随机位置

> -p2	结束位置，默认无

> -s	服务器地址, ip:port

> -w	手表的imei号


