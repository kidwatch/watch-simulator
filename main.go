package main

import (
	//"bytes"
	"bitbucket.org/kidwatch/watch"
	"flag"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

var (
	serverAddr     string
	imei           string
	point1, point2 string
	delta          float64
	interval       int
	heartbeat      int
	sendLBS        bool
	version        int
	once           bool
)

func init() {
	flag.StringVar(&serverAddr, "s", ":8000", "udp server address")
	flag.StringVar(&imei, "w", "", "watch's imei")
	flag.StringVar(&point1, "p1", "", "watch start location, default is random")
	flag.StringVar(&point2, "p2", "", "watch end location")
	flag.Float64Var(&delta, "d", 0, "location delta")
	flag.IntVar(&interval, "i", 30, "location upload interval time (s)")
	flag.IntVar(&heartbeat, "h", 30, "heartbeat send period (s)")
	flag.BoolVar(&sendLBS, "lbs", false, "send lbs instead of GPS")
	flag.IntVar(&version, "ver", 1, "kcp version")
	flag.StringVar(&watch.CipherServer, "cipher", "localhost:8899", "cipher server address")
	flag.BoolVar(&once, "once", false, "Run only once")
	flag.Parse()

	watch.ServerAddrs = strings.Split(serverAddr, ",")
}

func main() {
	rand.Seed(time.Now().UnixNano())

	var start, end []float64
	var lat, lng float64

	s := strings.Split(point1, ",")
	if len(s) == 2 {
		lat, _ = strconv.ParseFloat(s[0], 64)
		lng, _ = strconv.ParseFloat(s[1], 64)
	} else {
		lat = 30.0 + rand.Float64()
		lng = 120.0 + rand.Float64()
	}
	start = []float64{lat, lng}

	s = strings.Split(point2, ",")
	if len(s) == 2 {
		lat, _ = strconv.ParseFloat(s[0], 64)
		lng, _ = strconv.ParseFloat(s[1], 64)
		end = []float64{lat, lng}
	}

	w := watch.NewWatch(imei, watch.NewLocGenerator(start, end, delta))
	w.ProtoVersion = version
	run(w)
}

func run(w *watch.Watch) (err error) {
	if err = w.Login(); err != nil {
		return
	}
	defer w.Logout()

	go func() {
		if heartbeat == 0 {
			return
		}
		w.HeartBeat()
		ticker := time.NewTicker(time.Second * time.Duration(heartbeat))
		for {
			select {
			case <-ticker.C:
				w.HeartBeat()
			}
		}
	}()

	if sendLBS {
		w.SendLBS()
	} else {
		w.SendGPS()
	}
	ticker := time.NewTicker(time.Second * time.Duration(interval))
	for {
		select {
		case <-ticker.C:
			if sendLBS {
				w.SendLBS()
			} else {
				w.SendGPS()
			}
		}
		if once {
			break
		}
	}
	return
}
